/*
 Copyright 2014 James Hamilton <wjdhamilton@hotmail.co.uk>.
 */

package databasetools;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author M & D User
 */
public enum Commands
{
    WHERE("WHERE"),
    SELECT("SELECT"),
    FROM("FROM"),
    IGNORE("IGNORE"),
    EQUALS("="),
    INSERT("INSERT"),
    VALUES("VALUES"),
    INTO("INTO"),
    BETWEEN("BETWEEN"),
    AND("AND"), 
    OR("OR"),
    ORDER_BY("ORDER BY"),
    ASC("ASC"),
    DESC("DESC"),
    FETCH_FIRST("FETCH FIRST ROW ONLY"),
    FETCH_NEXT("FETCH NEXT"),
    ROWS_ONLY("ROWS ONLY"),
    OFFSET("OFFSET"),
    UPDATE("UPDATE"),
    GROUP_BY("GROUP BY"),
    SET("SET"), 
    TRUNCATE_TABLE("TRUNCATE TABLE");
    
    private final static Map<String, Commands> stringMap = new HashMap<>();
    
    static{
        for(Commands word : values()){
            stringMap.put(word.toString(), word);
        }
    }
    
    //the keyword to use. 
    private final String word;
    
    Commands(String word)
    {
        this.word = word;
    }
    
    /**
     * Returns the keyword with a space after it
     * @return 
     */
    public String insert()
    {
        return word+" ";
    }
    
    @Override
    public String toString()
    {
        return word;
    }
}
