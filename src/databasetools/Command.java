/*
 Copyright 2014 James Hamilton <wjdhamilton@hotmail.co.uk>.
 */

package databasetools;

import java.util.List;

/**
 *
 * @author M & D User
 */
public interface Command
{

    /**
     * Add the SELECT keyword to the statement without columns
     *
     * @return this
     */
    QueryBuilder select();

    /**
     * Add the SELECT keyword to the query, with a list of columns to select
     *
     * @param columns the columns to be selected
     * @return this
     */
    QueryBuilder select(List<String> columns);

    /**
     * Add the SELECT keyword to the query, with a single column argument.
     *
     * @param column the column to be selected
     * @return this
     */
    QueryBuilder select(String column);

    /**
     * Add the SELECT keyword to the statement with the columns specified in an
     * array
     *
     * @param columns
     * @return
     */
    QueryBuilder select(String[] columns);

    /**
     * Add the SELECT keyword with the parameter "*"
     *
     * @return this
     */
    QueryBuilder selectAll();
    
}
