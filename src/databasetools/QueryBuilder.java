package databasetools;

import static databasetools.Commands.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/*
 * Copyright 2014 James Hamilton <wjdhamilton@hotmail.co.uk>.
 */
/**
 * Immutable class that is intended to take the fiddliness out of generating SQL
 * queries.
 *
 * This class utilises a simulacrum of Joshua Bloch's builder pattern. Each
 * method returns an updated instance of a QueryBuilder which contains the most
 * up to date version of the query.
 *
 * The QueryBuilder also takes responsibility for ensuring that the query is
 * well-formed. If the client attempts to add a keyword in an inappropriate
 * place or use an invalid operator a runtime exception will be thrown. This is
 * an element of the system that will not be fully operational at first, but as
 * I get a feel for the language it should either improve, or be deprecated.
 *
 * SQL keywords are represented as methods, and parameters are the associated
 * parameters. The benefit of this approach is that the syntax and formatting of
 * the queries are handled in a uniform manner by the class, leaving less room
 * for error by the client.
 *
 * Calling a method, such as {@link #select(java.lang.String) } appends the
 * keyword, followed by the value argument(s) to the SQL statement followed by a
 * space. There is no requirement for the client to add a spaces between
 * keywords nor is there a requirement to add a semicolon at the end of the
 * statement.
 *
 * @author James Hamilton
 */
public class QueryBuilder
{

    //the connection that will be used by this query builder
    private final Statement stat;
    //the statement that will be built
    private final StringBuilder statement;
    private boolean isQuery;

    /**
     * Creates a new QueryBuilder for direct use with a connection.
     *
     * @param sqlStatement
     * @return
     * @throws SQLException
     */
    public static QueryBuilder newStatementConnected(Statement sqlStatement) throws SQLException
    {
        return new QueryBuilder(sqlStatement);
    }

    /**
     * Returns a QueryBuilder instance that does not have an internal connection
     * to the database.
     *
     * @return
     */
    public static QueryBuilder newStatement()
    {
        return new QueryBuilder();
    }

    /**
     * Instantiate a new QueryBuilder. This sets the (connection) autocommit
     * parameter to true.
     *
     * @param connection the connection that will execute the SQL statement
     * @throws java.sql.SQLException
     */
    private QueryBuilder(Statement connection) throws SQLException
    {
        stat = connection;
        statement = new StringBuilder();
        isQuery = false;
    }

    /**
     * Instantiate a new QueryBuilder. This sets the (connection) autocommit
     * parameter to true.
     *
     * @param connection the connection that will execute the SQL statement
     * @throws java.sql.SQLException
     */
    private QueryBuilder()
    {
        stat = null;
        statement = new StringBuilder();
        isQuery = false;
    }

    /**
     * Executes the query stored in this.
     *
     * @return the relevant ResultSet.
     * @throws java.sql.SQLException if the statement is not well formed
     */
    public ResultSet execute() throws SQLException, IllegalStateException
    {
        if (stat == null) {
            throw new IllegalStateException("No internal statement");
        }
        if (isQuery)//if the statement is of the query variety
        {
            return stat.executeQuery(statement.toString());
        } else {
            stat.executeUpdate(statement.toString(),Statement.RETURN_GENERATED_KEYS);
            return stat.getGeneratedKeys();
        }
    }

    /**
     * Add the SELECT keyword to the query, with a single column argument.
     *
     * @param column the column to be selected
     * @return this
     */
    public QueryBuilder select(String column)
    {
        appSel().append(column).append(" ");
        return this;
    }
    
    /**
     * Add the SELECT keyword to the query, with a single column argument.
     *
     * @param column1
     * @param column2
     * @return this
     */
    public QueryBuilder select(String column1, String column2)
    {
        appSel().append(column1).append(", ").append(column2).append(" ");
        return this;
    }

    /**
     * Add the SELECT keyword to the query, with a list of columns to select
     *
     * @param columns the columns to be selected
     * @return this
     */
    public QueryBuilder select(List<String> columns)
    {
        appSel();
        for (int i = 0; i < columns.size(); i++) {
            statement.append(columns.get(i));
            if (i != (columns.size() - 1)) {
                statement.append(", ");
            } else {
                statement.append(" ");
            }

        }
        isQuery = true;
        return this;
    }

    /**
     * Add the SELECT keyword with the parameter "*"
     *
     * @return this
     */
    public QueryBuilder selectAll()
    {
        appSel().append("* ");
        isQuery = true;
        return this;
    }

    /**
     * Add the SELECT keyword to the statement without columns
     *
     * @return this
     */
    public QueryBuilder select()
    {
        appSel();
        isQuery = true;
        return this;
    }

    /**
     * Add the SELECT keyword to the statement with the columns specified in an
     * array
     *
     * @param columns
     * @return
     */
    public QueryBuilder select(String[] columns)
    {
        appSel();
        statement.append(csvList(Arrays.asList(columns))).append(" ");
        isQuery = true;
        return this;
    }

    private StringBuilder appSel()
    {
        statement.append(SELECT.insert());
        return statement;
    }

    public QueryBuilder from(String table)
    {
        statement.append(FROM.insert()).append(table).append(" ");
        return this;
    }

    public QueryBuilder where(String column)
    {
        statement.append(WHERE.insert()).append(column).append(" ");
        return this;
    }

    public QueryBuilder equalTo(String argument)
    {
        statement.append(EQUALS.insert()).append(addQuotes(argument)).append(" ");
        return this;
    }

    /**
     * Append any String to the statement. Assumes that the SQL representation
     * will also be a String
     *
     * @param terms the terms to be added
     * @return this
     */
    public QueryBuilder addTerms(String terms)
    {
        statement.append(terms);
        return this;
    }

    /**
     * Insert the ignore keyword into a statement
     *
     * @return this
     */
    public QueryBuilder ignore()
    {
        statement.append(IGNORE.insert());
        return this;
    }

    /**
     * Add the {@code BETWEEN} keyword to the statement
     *
     * @param left the left hand argument
     * @param right the right hand argument
     * @return this
     */
    public QueryBuilder between(String left, String right)
    {
        statement.append(BETWEEN.toString()).append(" ")
                .append(addQuotes(left)).append(" ")
                .append(Commands.AND.toString()).append(" ")
                .append(addQuotes(right)).append(" ");
        return this;
    }

    public QueryBuilder and(String term)
    {
        statement.append(AND.insert()).append(" ").append(term).append(" ");
        return this;
    }

    public QueryBuilder or(String term)
    {
        statement.append(OR.insert()).append(term).append(" ");
        return this;
    }

    public QueryBuilder mod()
    {
        throw new UnsupportedOperationException();
    }

    /**
     * Carries out an insert statement using the values provided in colsVals where
     * key = column and value = data. 
     * 
     * This method assumes that all the values are strings. 
     * 
     * @param colsVals
     * @param table
     * @return 
     */
    public QueryBuilder insert(Map<String, String> colsVals, String table)
    {
        List<String> columns = new ArrayList<>();
        List<String> values = new ArrayList<>();
        statement.append(INSERT.insert()).append(INTO.insert()).append(table).append("(");
        colsVals.keySet().stream().forEach(column -> {
            columns.add(column);
            values.add(colsVals.get(column));
        });
        statement.append(csvList(columns)).append(")");
        statement.append(VALUES).append("(").append(csvList(values)).append(")");
        
        return this;
    }
    
    /**
     * Adds the phrase "FETCH FIRST ROW ONLY" to the query
     * @return this
     */
    public QueryBuilder fetchFirst()
    {
        statement.append(FETCH_FIRST.insert());
        return this;
    }
    
    /**
     * Adds the instruction to fetch a certain number of rows from the table
     * @param rowsToFetch the number of rows to fetch
     * @return this
     */
    public QueryBuilder fetchNext(int rowsToFetch)
    {
        statement.append(FETCH_NEXT.insert()).append(rowsToFetch).append(ROWS_ONLY.insert());
        return this;
    }
    
    public QueryBuilder orderBy(String column)
    {
        statement.append(ORDER_BY.insert()).append(column).append(" ");
        return this;
    }
    
    public QueryBuilder descending()
    {
        statement.append(DESC.toString());
        return this;
    }

    public QueryBuilder ascending()
    {
        statement.append(ASC.toString());
        return this;
    }
    
    public QueryBuilder truncate(String table)
    {
        statement.append(Commands.TRUNCATE_TABLE.insert()).append(table);
        return this;
    }

    /**
     * Returns the SQL statement stored in this
     *
     * @return
     */
    @Override
    public String toString()
    {
        return statement.toString();
    }

    /**
     * Takes a list of Strings and returns a string containing the values in
     * that list separated by commas. The final value is not followed by a
     * comma.
     *
     * @param values
     * @return
     */
    private StringBuilder csvList(List<String> values)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < values.size(); i++) {
            sb.append(values.get(i));
            if (i != values.size() - 1) {
                sb.append(",");
            }
        }
        return sb;
    }

    public static String addQuotes(String arg)
    {
        String start = arg;
        if(arg.contains("'")){
            int apostrophe = arg.indexOf("'");
            start = arg.substring(0, apostrophe);
            start += "'";
            start+=arg.substring(apostrophe);
        }
        String q = "'";
        return q + start + q;
    }

    public QueryBuilder update(Map<String, String> values, String table, String column, String match)
    {
       statement
               .append(UPDATE.insert())
               .append(table).append(" ")
               .append(csvList(getValueString(values)))
               .append(WHERE.insert())
               .append(column)
               .append(EQUALS.insert())
               .append(match);
       return this;       
    }
    
    public QueryBuilder groupBy(String column)
    {
        statement.append(GROUP_BY.insert()).append(column).append(" ");
        return this;
    }

    private static List<String> getValueString(Map<String, String> values)
    {
        return values.keySet().stream().map(columnKey ->{
            return SET.insert()+columnKey + EQUALS.insert() + values.get(columnKey);
        }).collect(Collectors.toList());
    }

}
