/*
 Copyright 2014 James Hamilton <wjdhamilton@hotmail.co.uk>.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author M & D User
 */
public class QueryBuilderTest
{

    public QueryBuilderTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    @Test
    public void testSomeMethod()
    {
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Goal: To assert that the Builder correctly builds and executes an SQL
     * Query. In this respect it should return the correct ResultSet for the
     * test query.
     */
    @Test
    public void testExecuteQuery()
    {

    }

    /**
     * Goal: To assert that the Builder correctly updates a table using an SQL
     * statement.
     */
    @Test
    public void testExecuteUpdate()
    {

    }
    
    /**
     * Goal: To assert that the Builder returns a well formed SELECT statement. 
     */
    @Test
    public void testSELECT()
    {
        
    }
    
    /**
     * Goal: to assert that the builder returns a well formed FROM statement
     */
    @Test
    public void testFROM()
    {
        
    }
    
    /**
     * Goal: to assert that the builder returns a well formed IN statement
     */
    @Test
    public void testIN()
    {
        
    }

}
